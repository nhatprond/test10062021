package com.example.test0606;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.decorator.Imploy;
import com.example.decorator.MClassMR;
import com.example.decorator.MCurrent;
import com.example.test0606.adapter.JapanAdaptee;
import com.example.test0606.adapter.MyAdapter;
import com.example.test0606.facady.Facady;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
    }

    private void initViews() {
//        Facady.getInstance().paymentByCashWithFreeShip("mail vip");
//
//        MyAdapter myAdapter=new MyAdapter(new JapanAdaptee());
//        myAdapter.send("xin chao");

        Imploy imploy=new MClassMR(new MCurrent("test"));
        imploy.doTask();
        imploy.joint();
    }
}