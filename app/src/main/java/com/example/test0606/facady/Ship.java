package com.example.test0606.facady;

import android.util.Log;

public class Ship {

    public void freeShipping(){
        Log.i("tag","FreeShipping");
    }
    public void expressShipping(){
        Log.i("tag","expressShipping");
    }
    public void standrardShipping(){
        Log.i("tag","standrardShipping");
    }
}
