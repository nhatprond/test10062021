package com.example.test0606.facady;

public class Facady {
    private static Facady instance=new Facady();
    private User user;
    private Pay pay;
    private Ship ship;

    private Facady() {
        user=new User();
        pay=new Pay();
        ship=new Ship();
    }
    public static Facady getInstance(){
       return instance;
    }

    public void paymentByCashWithFreeShip(String mail){
        user.getMail(mail);
        pay.byCash();
        ship.freeShipping();
    }
}
