package com.example.test0606.facady;

import android.util.Log;

public class Pay {

    public void byCard(){
        Log.i("tag","byCard");
    }
    public void byCreditCard(){
        Log.i("tag","byCreditCard");
    }
    public void byCash(){
        Log.i("tag","byCash");
    }
}
